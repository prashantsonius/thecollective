@Regression
Feature: check schema dependency form

  @schemaDependenciesForm
  Scenario: check credit card validation for address
    Given I am on web form
    When I select schema dependency
    Then Schema dependencies section appears
    When I enter credit card details
    Then billing address section should appear

  @ExpandForm
  Scenario: check clicking plus button adds new section
    Given I am on web form
    When I select schema dependency
    Then Schema dependencies section appears
    And I click on plus button
    Then new person section should appear

  @FormElementsCheck
  Scenario: check expected elements exists when form is loaded
    Given I am on web form
    When I select schema dependency
    Then Schema dependencies section appears
    And I check default values of Array of conditionals section

  @petAge
  Scenario Outline: Pet and its age selection
    Given I am on web form
    When I select schema dependency
    And I select pet selection as "<PetOption>"
    Then I check the result as per "<PetOption>" selection

    Examples:
      | PetOption          |
      | Yes: One           |
      | Yes: More than one |

  @removePerson
  Scenario: remove Extra Person Section
    Given I am on web form
    When I select schema dependency
    Then I remove person section by clicking red X