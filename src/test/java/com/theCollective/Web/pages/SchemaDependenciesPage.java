package com.theCollective.Web.pages;

import com.theCollective.helpers.WebCommonAction;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import static com.theCollective.steps.WebTestBase.driver;

public class SchemaDependenciesPage extends WebCommonAction {

    public SchemaDependenciesPage() {
        PageFactory.initElements(driver, this);
    }

    //Locators
    //region
    @FindBy(linkText = "Schema dependencies")
    private WebElement schemaDependenciesFormButton;

    @FindBy(id = "root_simple_credit_card")
    private WebElement enterCcDetails;

    @FindBy(xpath = "//*[@class='text-danger']")
    private WebElement errorMessage;

    @FindBy(id = "root_conditional_Do you have any pets?")
    private WebElement petSelection;

    @FindBy(id = "root_conditional_How old is your pet?")
    private WebElement petAge;

    @FindBy(xpath = "(//*[@class = 'control-label'])[8]")
    private WebElement petRemoval;

    @FindBy(xpath = "(//*[@class='field-radio-group']//input)[1]")
    private WebElement radioButton;

    @FindBy(id = "root_arrayOfConditionals__title")
    private WebElement arrayOfConditionals;

    @FindBy(id = "root_arrayOfConditionals_0__title")
    private WebElement person;

    @FindBy(xpath = "(//*[@class='glyphicon glyphicon-plus'])[1]")
    private WebElement btnAddPerson;

    @FindBy(id = "root_arrayOfConditionals_2__title")
    private WebElement thirdPersonTitle;

    @FindBy (xpath = "//*[@id='root_arrayOfConditionals']//button[3]")
    private WebElement redCross;

   //endregion

    public void clickOnSchemaDependenciesForm() {
        Assert.assertTrue(isElementDisplay(schemaDependenciesFormButton));
        schemaDependenciesFormButton.click();
    }

    public void enterCreditCardDetails() {
        enterCcDetails.sendKeys("4658858574857458");
    }

    public void message() {
        Assert.assertTrue(isElementDisplay(errorMessage));
    }

    public void selectPet(String dropDownSelection) {
        Select dropDown = new Select(petSelection);
        dropDown.selectByValue(dropDownSelection);
    }

    public void iSPetAgeTextFieldExist() {
        Assert.assertTrue(isElementDisplay(petAge));
        petAge.sendKeys("5");
    }

    public void petRemovalQuestion() {
        Assert.assertTrue(isElementDisplay(petRemoval));
        Assert.assertEquals("Do you want to get rid of any?*", (petRemoval.getText()));
        radioButton.click();
    }

    public void arrayOfConditionalsElements() {
        Assert.assertEquals("Array of conditionals", arrayOfConditionals.getText());
        Assert.assertEquals("Person*", person.getText());
    }

    public void clickPlusButtonToAddPerson() {
        btnAddPerson.click();
    }

    public void thirdPersonSectionShouldAppear() {
        Assert.assertTrue(thirdPersonTitle.isDisplayed());
    }

    public void clickRemovePerson(){
        Assert.assertTrue(isElementDisplay(redCross));
        redCross.click();
        Assert.assertTrue(isElementDisappeared(redCross));

    }
}

