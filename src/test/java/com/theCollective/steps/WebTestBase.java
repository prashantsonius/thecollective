package com.theCollective.steps;
import com.theCollective.helpers.WebCommonAction;
import io.cucumber.core.api.Scenario;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.concurrent.TimeUnit;



public class WebTestBase extends WebCommonAction{

	public static WebDriver driver;
	private static String chromeDriverMacPath = System.getProperty("user.dir")
			+ "/src/test/resources/chromeDriver/chromedriver.exe";

	@Before
	public void startApp() {
			System.setProperty("webdriver.chrome.driver", chromeDriverMacPath);
			driver = new ChromeDriver();
			Point targetPosition = new Point(0, 0);
			driver.manage().window().setPosition(targetPosition);
			Dimension targetSize = new Dimension(1920, 1080);
			driver.manage().window().setSize(targetSize);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	}

	@After
	public void closeApp(Scenario scenario) {

		if (scenario.isFailed()) {
			try {
				System.out.println("Failed scenario ----->> "+scenario.getSourceTagNames());
				byte[] screenshot = ((TakesScreenshot) driver)
						.getScreenshotAs(OutputType.BYTES);
				scenario.embed(screenshot, "image/png");

			} catch (WebDriverException somePlatformsDontSupportScreenshots) {
				System.err.println(somePlatformsDontSupportScreenshots
						.getMessage());
			}
		}
		System.out.println("Reaching Closing BaseDriver");
		driver.quit();
	}

}
