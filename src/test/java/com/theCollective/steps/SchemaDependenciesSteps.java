package com.theCollective.steps;

import com.theCollective.Web.pages.SchemaDependenciesPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static com.theCollective.helpers.WebCommonAction.site;
import static com.theCollective.steps.WebTestBase.driver;

public class SchemaDependenciesSteps {

    private SchemaDependenciesPage schemaDependenciesPage = new SchemaDependenciesPage();

    @Given("I am on web form")
    public void iAmOnWebForm() {
        driver.get(site);
        driver.manage().window().maximize();
    }

    @When("I select schema dependency")
    public void iSelectSchemaDependency() {
        schemaDependenciesPage.clickOnSchemaDependenciesForm();
    }

    @Then("Schema dependencies section appears")
    public void schemaDependenciesSectionAppears() {
    }

    @When("I enter credit card details")
    public void iEnterCreditCardDetails() {
        schemaDependenciesPage.enterCreditCardDetails();
    }

    @Then("billing address section should appear")
    public void billingAddressSectionShouldAppear() {
        schemaDependenciesPage.message();
    }

    @And("I select pet selection as {string}")
    public void iSelectPetSelectionAs(String dropDownOption) {
        schemaDependenciesPage.selectPet(dropDownOption);
    }

    @Then("I check the result as per {string} selection")
    public void iCheckTheResultAsPerSelection(String petOption) {
        if (petOption.equalsIgnoreCase("Yes: One")) {
            schemaDependenciesPage.iSPetAgeTextFieldExist();

        } else {
            schemaDependenciesPage.petRemovalQuestion();
        }
    }

    @And("I check default values of Array of conditionals section")
    public void iCheckDefaultValuesOfArrayOfConditionalsSection() {
        schemaDependenciesPage.arrayOfConditionalsElements();
    }

    @And("I click on plus button")
    public void iClickOnPlusButton() {
        schemaDependenciesPage.clickPlusButtonToAddPerson();
    }

    @Then("new person section should appear")
    public void newPersonSectionShouldAppear() {
        schemaDependenciesPage.thirdPersonSectionShouldAppear();
    }

    @Then("I remove person section by clicking red X")
    public void iRemovePersonSectionByClickingRedX() {
schemaDependenciesPage.clickRemovePerson();
    }
}
