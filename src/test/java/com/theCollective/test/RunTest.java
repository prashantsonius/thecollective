package com.theCollective.test;


import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/features/",
        plugin = {"pretty","html:target/cucumber-html-report"},
        glue = {"com.theCollective.steps"},
        tags = {"@removePerson"})
public class RunTest {

}


