package com.theCollective.helpers;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import java.time.Duration;


import static com.theCollective.steps.WebTestBase.driver;

public class WebCommonAction {
    public static final String site = System.getProperty("site");
    public boolean isElementDisplay(WebElement display_ele) {
        try {
            Wait<WebDriver> wait1 = new FluentWait<WebDriver>(driver)
                    .withTimeout(Duration.ofSeconds(15))
                    .pollingEvery(Duration.ofSeconds(1))
                    .ignoring(NoSuchElementException.class)
                    .ignoring(TimeoutException.class);
            wait1.until(ExpectedConditions.visibilityOf(display_ele));

            return display_ele.isDisplayed();

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean isElementDisappeared(WebElement display_ele) {
        try {
            Wait<WebDriver> wait1 = new FluentWait<WebDriver>(driver)
                    .withTimeout(Duration.ofSeconds(5))
                    .pollingEvery(Duration.ofSeconds(1))
                    .ignoring(TimeoutException.class);
            wait1.until(ExpectedConditions.invisibilityOf(display_ele));

            return !display_ele.isDisplayed();
        } catch (NoSuchElementException | StaleElementReferenceException nse) {
            return true;
        }
    }
    
}
