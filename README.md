# TheCollective-Test

**Pre-requisites**

Java 1.8
Maven
Cucumber

**Setting up selenium-cucumber-java**

Install Java and set path.
Install Maven and set path.
Clone this repository or download zip.

**Running features**

Goto project directory on terminal.

Use "mvn test" command to run features.



